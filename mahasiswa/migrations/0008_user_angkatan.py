# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-12 17:54
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mahasiswa', '0007_merge_20171212_2101'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='angkatan',
            field=models.IntegerField(default=2016, validators=[django.core.validators.MaxValueValidator(2018), django.core.validators.MinValueValidator(1900)]),
        ),
    ]
