from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.
class User(models.Model):
    name = models.CharField(max_length=250, default="Empty")
    npm = models.CharField(max_length=15, default="Empty")
    angkatan = models.IntegerField(default=2016,
        validators=[MaxValueValidator(2018), MinValueValidator(1900)])
    npm = models.CharField(max_length=15, default="Empty")
    email = models.TextField(default="Empty")
    link = models.TextField(default="https://cdn1.iconfinder.com/data/icons/user-pictures/100/male3-512.png")
    linkedIn = models.TextField(default="Empty")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    expert = models.CharField(max_length=300, default="Java, C++")

# class Expertise(models.Model):
#     user = models.ForeignKey(User)
#     expert = models.CharField('Expertise', max_length=27)
#     created_at = models.DateTimeField(auto_now_add=True)
#     updated_at = models.DateTimeField(auto_now=True)

class Status(models.Model):
    description = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)
    user_id = models.CharField(max_length=15, default="1606917922")
    user_name = models.CharField(max_length=250, default="Empty")
