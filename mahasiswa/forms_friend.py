from django import forms

class Friend_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    description_attrs = {
        'type': 'text',
        'rows': 3,
        'class': 'form-control',
        'placeholder':'Cari Mahasiswa'
    }

    search = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))
