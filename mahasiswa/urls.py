from django.conf.urls import url
from .views import index, profile, edit, add_status, delete_status, add_bot_friends, delete_friend, search_friend, update
from .custom_auth import auth_login, auth_logout
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^profile/$', profile, name='profile'),
    url(r'^update/', update, name='update'),
    url(r'^edit/$', edit, name='edit'),
    url(r'^add_status', add_status, name='add_status'),
    url(r'^delete_status/(?P<id_status>\d+)/$', delete_status, name = 'delete_status'),
    url(r'^custom_auth/login/$', auth_login, name='auth_login'),
    url(r'^custom_auth/logout/$', auth_logout, name='auth_logout'),
    url(r'^add_bot_friends', add_bot_friends, name='add_bot_friends'),
    url(r'^delete_friend/(?P<id>\d+)/$', delete_friend, name = 'delete_friend'),
    url(r'^search_friend', search_friend, name = 'search_friend'),
]
