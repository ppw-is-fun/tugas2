from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, profile

class Tugas2UnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/mahasiswa/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_func(self):
        found = resolve('/mahasiswa/')
        self.assertEqual(found.func, index)