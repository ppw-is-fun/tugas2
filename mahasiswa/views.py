from __future__ import unicode_literals
from django.db.models import Q
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from .models import User, Status
from .forms import Status_Form
from .forms_friend import Friend_Form
from random import randint
from django.core import serializers
import json
# Create your views here.

response = {}
def index(request): # pragma: no cover
    # response['form'] = "aaaa" #Status_Form
    if 'user_login' in request.session:
        response['status_form'] = Status_Form
        html = 'tugas2/tables/status.html'
        render(request, html, response)
        return HttpResponseRedirect(reverse('mahasiswa:profile'))
    else:
        html = 'tugas2/session/login.html'
        return render(request, html, response)

def add_status(request): # pragma: no cover
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['description'] = request.POST['description']
        status = Status(description=response['description'],user_id=request.session['kode_identitas'], user_name=request.session['user_login'])
        status.save()
        #html ='lab_4/form_result.html'
        return HttpResponseRedirect('/mahasiswa/')
    else:
        return HttpResponseRedirect('/mahasiswa/')

def delete_status(request, id_status): # pragma: no cover
    status = Status.objects.get(pk = id_status)
    status.delete()
    return HttpResponseRedirect('/mahasiswa/')

@csrf_exempt
def update(request):
    if request.method == 'POST':
        for key in request.session.items():
            print (key)
        
        kode_identitas = request.session['kode_identitas']
        profile = User.objects.get(pk=1)
        fullname = request.POST['full_name']
        url_linkedin = request.POST['url_linkedin']
        picture_url = request.POST['picture_url']
        email = request.POST['email']
        profile.name = fullname
        profile.linkedIn = url_linkedin
        profile.link = picture_url
        profile.email = email
        profile.save()
        return HttpResponseRedirect(reverse('mahasiswa:profile'))


def profile(request): # pragma: no cover
    profile_tampilan = User(name="Empty", email="Empty", link="https://cdn1.iconfinder.com/data/icons/user-pictures/100/male3-512.png")
    profile_tampilan.save()
    profile = User.objects.get(pk=1)
    splitexp = profile.expert.split(',')
    status = Status.objects.filter(user_id=request.session['kode_identitas'])
    response = {'profile': profile}
    response['friend_list'] = User.objects.filter(~Q(name="Empty"))
    response['form'] = Status_Form
    response['form2'] = Friend_Form
    response['model'] = status
    html = 'tugas2/session/profile.html'
    profile.name = request.session['user_login']
    friend = User.objects.filter(~Q(name = "Empty"))
    response['friend_list'] = friend
    response['splitexp'] = splitexp
    html = 'tugas2/session/profile.html'
    for key in request.session.items():
        print (key)
    profile.npm = request.session['kode_identitas']
    response['jml_status'] = len(status)
    return render(request, html, response)

def edit(request): # pragma: no cover
    profile_tampilan = User(name="Empty", email="Empty", link="https://cdn1.iconfinder.com/data/icons/user-pictures/100/male3-512.png")
    profile_tampilan.save()
    profile = User.objects.get(pk=1)
    response= {'profile': profile}
    status = Status.objects.filter(user_id=profile.npm)
    response['form'] = Status_Form
    response['model'] = status
    html = 'tugas2/session/profile.html'
    for key in request.session.items():
        print (key)
    profile.name = request.session['user_login']
    profile.npm = request.session['kode_identitas']
    response['jml_status'] = len(status)
    html = 'tugas2/session/edit.html'
    return render(request, html, response)

# def friend_List(request):
#     add_bot_friends(request)
#     list_Friend = User.objects.all()
#     response['friend_list'] = list_Friend
#     html = "tugas2/tables/mahasiswa/tabel_teman.html"
#     return render(request, html, response)

def add_bot_friends(request): # pragma: no cover
    print(request.method)
    if request.method == "POST" :
        nama = ["Baba", "Bubu", "kolal", "Cinta", "bubu", "Bibi", "Ade", "huhu", "Kak Pewe", "Asdos"]
        npm = ["1092872138", "21378871", "213672131", "1283712", "172633", "1233412", "1233123", "123321342", "12342141", "1234564"]
        keahlian = ["Java", "Python", "C++", "HTML", "C#"]
        for x in range(10):
            friend = User(name=nama[x], npm=npm[x], expert=keahlian[randint(0,4)])
            friend.save()
            print("Berhasil tambah")
        print("babababa")
        return HttpResponseRedirect('/mahasiswa/')
    else:
        return HttpResponseRedirect('/mahasiswa/')


def delete_friend(request, id): # pragma: no cover
    friend = User.objects.get(pk = id)
    friend.delete()
    return HttpResponseRedirect('/mahasiswa/')

def search_friend(request): # pragma: no cover
    form = Friend_Form(request.POST or None)
    users = User.objects.filter(name="Asdos")
    if(request.method == 'POST' and form.is_valid()):
        sesuatu = request.POST["Filter"]
        if sesuatu == "Nama":
            users = User.objects.filter(name = request.POST["search"])
        elif sesuatu == "NPM":
            users = User.objects.filter(npm=request.POST["search"])
        elif sesuatu == "Angkatan":
            users = User.objects.filter(angkatan=request.POST["search"])
        elif sesuatu == "Keahlian":
            users = User.objects.filter(expert=request.POST["search"])
        html = 'tugas2/tables/mahasiswa/searchsct.html'
        response["friends"] = users
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/mahasiswa/profile/')

# def get_user_data(sesuatu, id):

#     data = []
#     for user in users:
#         temp = {}
#         temp["name"] = user.name
#         temp["npm"] = user.npm
#         temp["angkatan"] = user.angkatan
#         temp["keahlian"] = user.expertise
#         data.append(temp)
#     return json.dumps(data)